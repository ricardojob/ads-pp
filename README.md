# Práticas de Programação

* **Curso:** [Análise e Desenvolvimento de Sistemas - IFPB Cajazeiras](http://www.ifpb.edu.br/campi/cajazeiras/cursos/cursos-superiores-de-tecnologia/analise-e-desenvolvimento-de-sistemas)
* **Professor:** Ricardo Job
* **Precisando de Ajuda?**
    * [Email do grupo](mailto:pp-20142@googlegroups.com) para comunicados entre professor e alunos.
	* [Email](mailto:ricardo.job@ifpb.edu.br) para dúvidas pontuais, ou para marcar uma reunião.
* **[Material da Disciplina](https://drive.google.com/folderview?id=0B3bNBHsD1S9gZXRJYXpkVXZQRkE) **
* **[Notas da Disciplina](https://docs.google.com/spreadsheets/d/1aIEin_1AdX4p6DQQv9ebNDRyTQnG5Mk8TA0jxdppnyA)**
## Descrição da Disciplina


## Objetivo Geral

Permitir o aprendizado de conceitos e técnicas fundamentais necessários à integração, automatização e construção de aplicações.

## Objetivos Específicos

* Entender os fundamentos da integração de software;
* Aplicar e gerenciar os principais frameworks utilizados do desenvolvimento de um sistema;
* Entender e aplicar uma Modelagem de Software;
* Conhecer e utilizar os principais conceitos de Teste;
* Entender e utilizar os principais conceitos de Projeto Arquitetural; 

## Avaliação

* Projeto 1 (Primeira Parte) – 30%
* Projeto 2 (Segunda Parte) – 30%
* Atividade Contínuas – 40%

## Conteúdo Programático

1. Apresentação da Disciplina
	* Data: 09/09/2014 - (3 aulas)
	* Apresentação da disciplinas, método de avaliação e cronograma de conteúdos.	
2. Ant
	* Data: 16/09/2014 -(3 aulas)
	* Ant: Definição e motivação para usar uma ferramenta de construção.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gcVlKN2lBZXYtbUU/)
3. Ant
	* Data: 19/09/2014 - (3 aulas)
	* Ant: Definição e exemplos.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gcVlKN2lBZXYtbUU/)
	* [Exemplos](https://bitbucket.org/ricardojob/ads-pp-ant)	
4. Ant
	* Data: 23/09/2014 - (3 aulas)
	* Ant: Exemplos: Compilar, executar, construir um arquivo Jar.
	* Atividade: Próxima aula construir uma aplicação web e implantar no tomcat.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gcVlKN2lBZXYtbUU/)
	* [Exemplos](https://bitbucket.org/ricardojob/ads-pp-ant)	
			
## Bibliografia
* Geary, D.; Horstmann, C.  Core Java Server Faces.  Alta Books, 2ª edição, 2007.  
* Clarke, J.; Connors, J.;  Eric B. Java FX - Desenvolvendo Aplicações de Internet Ricas. Alta Books, 1ª edição, 2010.  
* Jacobi, J. Pro JSF e AJAX - Construindo componentes ricos para a internet. Ciência Moderna, 4ª edição, 2007.
* Weaver, J. L.; Gao, W.; Chin, S.; Iverson, D.  Plataforma Pro JavaFX - Desenvolvimento de RIA para Dispositivos Móveis e para Área de Trabalho por Scripts com a Tecnologia Java. Ciência Moderna, 1ª edição, 2010.
* Gonçalves, E. Dominando Relatórios JasperReport com iReport. Ciência Moderna, 1ª edição, 2008.
 

